using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delegates2001
{
    class Program 
    {
        static void MyForEachLinq(int x)
        {
            Console.WriteLine(x);
        }
        // *Etgar
        static void MyForeachZugi<T>(List<T> list, Action<T> foreachAction)
        {
            for (int i = 0; i < list.Count; i++)
            {
                if (i % 2 == 0)
                {
                    foreachAction(list[i]);
                }
            }
        }
        static void MyForeach(List<int> list, Action<int> foreachAction)
        {
            foreach (var item in list)
            {
                foreachAction(item);
            }
        }
        static bool WhereFuncForPositive(int x)
        {
            return x > 0;
        }
        static List<int> MyWhere(List<int> list, Func<int, bool> whereFunc)
        {
            List<int> result = new List<int>();
            foreach (var item in list)
            {
                if (whereFunc(item))
                {
                    result.Add(item);
                }
            }
            return result;
        }
        static List<T> MyWhereGeneric<T>(List<T> list, Func<T, bool> whereFunc)
        {
            List<T> result = new List<T>();
            foreach (var item in list)
            {
                if (whereFunc(item))
                {
                    result.Add(item);
                }
            }
            return result;
        }
        static void Main(string[] args)
        {
            List<int> list = new List<int>() { 1, -2, 3, -4, 6, -100, 2, 3 };
            List <double> list_double = new List<double>() { -0.979, 4.1232, 3.17, 80.0 };
            //list.ForEach(MyForEachLinq);
            list.ForEach(x => Console.WriteLine(x) );
            MyForeachZugi<int>(list, x => Console.WriteLine(x));
            MyForeach(list, x => Console.WriteLine(x));

            List<int> result_list = list.Where(WhereFuncForPositive).ToList();
            List<int> result_list1 = list.Where(x => x > 0).ToList();
            List<int> result_list2 = MyWhere(list, x => x > 0);
            List<double> result_list3 = MyWhereGeneric<double>(list_double, x => x > 0);
            List<double> result_list4 = MyWhereGeneric(list_double, x => x > 0);
            
            // TODO:
            // use all 5 with lambda + check results!
            // 1 + 2 + 3 => write methods that do the same (*etgar: also Generic)
            // list.Find
            // list.FindAll
            // list.FindIndex
            // list.FindLast
            // list.FindLastIndex
            
        }
    }
}
