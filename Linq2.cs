using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delegates2001
{
    class Program 
    {
        static void MyForEachLinq(int x)
        {
            Console.WriteLine(x);
        }
        // *Etgar
        static void MyForeachZugi<T>(List<T> list, Action<T> foreachAction)
        {
            for (int i = 0; i < list.Count; i++)
            {
                if (i % 2 == 0)
                {
                    foreachAction(list[i]);
                }
            }
        }
        static void MyForeach(List<int> list, Action<int> foreachAction)
        {
            foreach (var item in list)
            {
                foreachAction(item);
            }
        }
        static bool WhereFuncForPositive(int x)
        {
            return x > 0;
        }
        static List<int> MyWhere(List<int> list, Func<int, bool> whereFunc)
        {
            List<int> result = new List<int>();
            foreach (var item in list)
            {
                if (whereFunc(item))
                {
                    result.Add(item);
                }
            }
            return result;
        }
        static List<T> MyWhereGeneric<T>(List<T> list, Func<T, bool> whereFunc)
        {
            List<T> result = new List<T>();
            foreach (var item in list)
            {
                if (whereFunc(item))
                {
                    result.Add(item);
                }
            }
            return result;
        }
        static int MyFind(List<int> list, Func<int, bool> f)
        {
            foreach (var item in list)
            {
                if (f(item))
                {
                    return item;
                }
            }
            return 0;
        }
        static T MyFind<T>(List<T> list, Func<T, bool> f)
        {
            foreach (var item in list)
            {
                if (f(item))
                {
                    return item;
                }
            }
            return default(T);
        }
        class Person
        {
            public string Id { get; set; }
            public string Name { get; set; }
            public int Age { get; set; }
        }
        static void Change(int x)
        {
            x = 0;
        }
        static bool Check_if_n_items_pass_condition(List<int> list, int how_many_times, Predicate<int> f)
        {
            int counter = 0;
            foreach (var item in list)
            {
                if (f(item))
                {
                    counter++;
                    if (counter >= how_many_times)
                        return true;
                }
            }
            return false;
        }

        // The following method should return true if each element in the squares sequence
        // is equal to the square of the corresponding element in the numbers sequence.
        // Try to write the entire method using only LINQ method calls, and without writing
        // any loops.
        public static bool TestForSquares(IEnumerable<int> numbers, IEnumerable<int> squares)
        {
            return false;
        }
        // Given a sequence of words, get rid of any that don't have the character 'e' in them,
        // then sort the remaining words alphabetically, then return the following phrase using
        // only the final word in the resulting sequence:
        //    -> "The last word is <word>"
        // If there are no words with the character 'e' in them, then return null.
        //
        // TRY to do it all using only LINQ statements. No loops or if statements.
        public static string GetTheLastWord(IEnumerable<string> words)
        {
            return false;
        }

        static void Main(string[] args)
        {
            List<int> list = new List<int>() { 1, -2, 3, -4, 6, -100, 2, 3 };
            for (int i = 0; i < list.Count ; i++)
            {
                Change(list[i]);
            }

            List <double> list_double = new List<double>() { -0.979, 4.1232, 3.17, 80.0 };
            //list.ForEach(MyForEachLinq);
            list.ForEach(x => Console.WriteLine(x) ); // eager
            MyForeachZugi<int>(list, x => Console.WriteLine(x));
            MyForeach(list, x => Console.WriteLine(x));

            List<int> result_list = list.Where(WhereFuncForPositive).ToList();
            List<int> result_list1 = list.Where(x => x > 0).ToList();
            List<int> result_list2 = MyWhere(list, x => x > 0);
            List<double> result_list3 = MyWhereGeneric<double>(list_double, x => x > 0);
            List<double> result_list4 = MyWhereGeneric(list_double, x => x > 0);

            // TODO:
            // use all 5 with lambda + check results!
            // 1 + 2 + 3 => write methods that do the same (*etgar: also Generic)
            // list.Find
            int first_positive = list.Find(x => x > 0);

            // list.FindAll
            List<int> items_bigger_10 = list.FindAll(x => x > 10);
            List<int> items_bigger_10_where = list.Where(x => x > 10).ToList();

            // from all numbers get smaller than 0 and then first bigger than -50
            // concat
            int res = list.Where(x => x < 0).First(x => x < -50);

            // list.FindIndex
            int first_index = list.FindIndex(x => x > 0);
            int last_index = list.FindLastIndex(x => x > 0);

            // list.FindLast
            // list.FindLastIndex
            bool are_all_items_larger_0 = list.All(x => x > 0);

            int max = list.Where(x => x < 0).Max();
            int min = list.Where(x => x > 0).Min();
            double avg = list.Where(x => x > 0).Average();

            List<int> double_size = list.Select(_ => _ * 2).ToList();
            List<Person> people = new List<Person>(); // add some person
            List<string> ids = people.Select(_ => _.Id).ToList();
            people.ForEach(p => p.Id = "NA");
            // Lazy vs eager

            //List<int> list = new List<int>() { 1, -2, 3, -4, 6, -100, 2, 3 };
            IEnumerable<int> all_negative = list.Where(x => x < 0); // Lazy
            for (int i = 0; i < list.Count; i++)
            {
                list[i] = list[i] < 0 ? 0 : list[i];
            }
            List<int> result_negative =  all_negative.ToList();

            List<double> doubles = new List<double>() { -0.979, 4.1232, 3.17, 80.0 };
            List<int> ints1 = doubles.Select(_ => (int)Math.Round(_)).ToList();
           // List<int> ints3 = doubles.Cast<int>().ToList();

            List<Person> people2 = doubles.Select(_ => (int)Math.Round(_)).ToList().Select(_ => new Person
            {
                Id = _.ToString(),
                Name = "Incognito"
            }).ToList();

            // take list of double
            // take the double { -0.979, 4.1232, 3.17, 80.0 } and create list of anonymous objects 
            // with : original , AbsValue , Power2
            // print this list --> try to do all this in one line

            IEnumerable<double> lazy = doubles.Where(_ => _ > 4);
            for (int i = 0; i < doubles.Count; i++)
            {
                doubles[i] = 0;
            }
            List<double> res_ = lazy.ToList();
        }
    }
}
